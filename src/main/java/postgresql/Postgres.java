package postgresql;

import java.sql.*;
public class Postgres {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
//            Class.forName("com.mysql.jdbc.Driver");
             Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ce) {
            System.out.println("No driver");
        }
        try {
//            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/july", "root", "root");
             conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/common", "postgres", "root");

            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from august");
            while (rs.next()) {
                int id = rs.getInt(1);
                String col1 = rs.getString(2);
                String col2 = rs.getString(3);
                System.out.println(id + " " + col1 + " " + col2 + " ");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
                conn.close();
                stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
